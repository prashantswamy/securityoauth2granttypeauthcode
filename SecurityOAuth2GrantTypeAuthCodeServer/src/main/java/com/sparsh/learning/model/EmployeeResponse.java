package com.sparsh.learning.model;

import java.util.List;

import lombok.Data;

@Data
public class EmployeeResponse {
	private List<Employee> employees;
}