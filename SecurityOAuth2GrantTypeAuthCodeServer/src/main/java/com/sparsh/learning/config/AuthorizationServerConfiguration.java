package com.sparsh.learning.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

		// @formatter:off
		clients.inMemory()
				.withClient("prashant")
				.secret("{noop}secret")
				.authorizedGrantTypes("authorization_code")
				.scopes("read")
				.authorities("CLIENT")
				.redirectUris("http://localhost:8090/showEmployees")
				;
		// @formatter:off
	}
}