package com.sparsh.learning.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class OAuth2SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// @formatter:off
        http.authorizeRequests()
        	.antMatchers("/").permitAll()
        	.antMatchers("/user/getEmployeesList").hasAnyRole("ADMIN").anyRequest().authenticated()
        	.and().formLogin().permitAll()
        	.and().logout().permitAll();
        // @formatter:off

        http.csrf().disable();
    }
	
    @Override
    public void configure(AuthenticationManagerBuilder authenticationMgr) throws Exception {
    	
    	PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        authenticationMgr.inMemoryAuthentication() //
        				.withUser("admin") //
        				.password(encoder.encode("admin")) //
        				.roles("ADMIN") //
        				.authorities("ROLE_ADMIN");
    }
}
