package com.sparsh.learning.controllers;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sparsh.learning.model.Employee;

@Controller
public class EmployeeController {

	@RequestMapping(value = "/getEmployees", method = RequestMethod.GET)
	public ModelAndView getEmployeeInfo() {
		return new ModelAndView("getEmployees");
	}

	@RequestMapping(value = "/showEmployees", method = RequestMethod.GET)
	public ModelAndView showEmployees(@RequestParam("code") String code) throws JsonProcessingException, IOException {
		ResponseEntity<String> response = null;
		System.out.println("Authorization Code------" + code);

		RestTemplate restTemplate = new RestTemplate();

		String credentials = "prashant:secret";
		String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Authorization", "Basic " + encodedCredentials);

		HttpEntity<String> request = new HttpEntity<String>(headers);

		StringBuilder accessTokenUrl = new StringBuilder("http://localhost:8080/oauth/token");
		accessTokenUrl.append("?code=").append(code);
		accessTokenUrl.append("&grant_type=authorization_code");
		accessTokenUrl.append("&redirect_uri=http://localhost:8090/showEmployees");

		response = restTemplate.exchange(accessTokenUrl.toString(), HttpMethod.POST, request, String.class);

		System.out.println("Access Token Response ---------" + response.getBody());

		// Get the Access Token From the received JSON response
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(response.getBody());
		String token = node.path("access_token").asText();

		String url = "http://localhost:8080/user/getEmployeesList";

		// Use the access token for authentication
		HttpHeaders headers1 = new HttpHeaders();
		headers1.add("Authorization", "Bearer " + token);
		headers1.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers1.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<>(headers1);

		/*
		 * List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>(); // Add the Jackson Message converter
		 * MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter(); // Note: here we are making this converter to
		 * process any kind of response, // not only application/*json, which is the default behaviour
		 * converter.setSupportedMediaTypes(Arrays.asList(MediaType.ALL)); messageConverters.add(converter);
		 * restTemplate.setMessageConverters(messageConverters);
		 */

		ResponseEntity<List<Employee>> employees = restTemplate.exchange(url, HttpMethod.GET, entity,
				new ParameterizedTypeReference<List<Employee>>() {
				});

		// ResponseEntity<String> employees = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

		System.out.println(employees);
		List<Employee> employeeArray = employees.getBody();

		ModelAndView model = new ModelAndView("showEmployees");
		model.addObject("employees", employeeArray);
		return model;
	}
}