#Spring Boot2 OAuth 2 Authorization Code Grant Type Example

Server application => SecurityOAuth2GrantTypeAuthCodeServer
Client application => SecurityOAuth2GrantTypeAuthCodeClient

Run both applications and try with below URL

http://localhost:8090/getEmployees

use below credentials

admin/admin


This application is tested with Java 1.8 and versions mentioned in pom.xml. It not working if upgraded the JDK version and framework versions.